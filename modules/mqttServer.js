const mosca = require('mosca');
let mqtt_server;
const watchdogData = {};

module.exports = {
	init() {
		return new Promise((resolve, reject) => {
			mqtt_server = new mosca.Server({
				host: serverParams.ip,
				port: serverParams.mqtt_port,
				http: {
					port: serverParams.mqtt_http_port
				}
			})
			mqtt_server
				.on('clientConnected', (client) => {
					log('client connected   ', [client.id]);
				})
				.on('clientDisconnected', (client) => {
					log('client disconnected', [client.id]);
				})
				.on('subscribed', (topic, client) => {
					try {
						log('client subscribed  ', [client.id, topic]);
						const topic_s = topic.split('/');
						if(topic_s.length==3) {
							let data = {};
							const state = db.getState();
							const obj = state[topic_s[0]][topic_s[1]][topic_s[2]];
							for (let key in obj) {
								if(['mode','value','preferences'].indexOf(key)!=-1) {
									data[key] = obj[key];
								}
							};
							this.publish(topic, data);
						}
					} catch(err) {log(err)}
				})
				.on('published', (packet, client) => {
					try {
						if(client) {
							const client_id = client.id;
							const topic = packet.topic;
							log('client published   ', [client_id, topic, packet.payload.toString()]);
							const payload = JSON.parse(packet.payload);
							if(topic=='custom-events' && customEvents[payload.event]) {
								customEvents[payload.event](payload);
							} else
							if (topic=='watchdog/data') {
								if (watchdogData[client_id]) {
									clearTimeout(watchdogData[client_id]);
								}
								watchdogData[client_id] = setTimeout(() => {
									this.publish('watchdog/reboot', { reason: client_id });
								}, 2 * 60 * 1000);
							} else
							if(db.updateStateByTopic(topic, payload, false)) {
								check_motion(topic, payload);
							}
						}
					} catch(err) {log(err)}
				})
				.on('ready', () => {
					log(`MQTT server ready at ${serverParams.ip}:${serverParams.mqtt_port}`);
					resolve();
				})
		})
	},
	publish(topic, payload) {
		try {
			const message = {
				topic: topic,
				payload: JSON.stringify(payload)
			};
			mqtt_server.publish(message, () => {
				log('server published   ', [message.topic, message.payload]);
			});
		} catch(err) {log(err)}
	}
}

const motionTopics = {
	'toilet/data': {
		data: (state) => state.toilet.data,
		target: (state) => state.toilet.controls.light,
		url: 'toilet/controls/light'
	},
	'bathroom/data': {
		data: (state) => state.bathroom.data,
		target: (state) => state.bathroom.controls.light,
		url: 'bathroom/controls/light'
	},
	'hall/data': {
		data: (state) => state.hall.data,
		target: (state) => state.hall.controls.light2,
		url: 'hall/controls/light2'
	}
}

const customEvents = {
	muteLight(payload) {
		const state = db.getState();
		for (let key in state) {
			if(!payload.page || payload.page==key) {
				for (let key2 in state[key].controls) {
					const value2 = state[key].controls[key2];
					if(value2.preferences.mute==1 && value2.modes[value2.mode]=='on') {
						db.updateStateByTopic(key+'/controls/'+key2, {mode: value2.modes.indexOf('off')}, true);
					}
				}
			}
		}
	},
	motionEvent(topic, motion) {
		const currentTopic = motionTopics[topic];
		if(currentTopic) {
			const target = currentTopic.target(db.getState());
			if(target.modes[target.mode]=='auto') {
				const hour = (new Date(Date.now())).getHours();
				let value = 0;
				if(motion==1) {
					if(hour>=target.preferences.night_start || hour<target.preferences.night_end) {
						value = target.preferences.night_value;
					} else {
						value = 100;
					}
				}
				if(value!=target.value) {
					db.updateStateByTopic(currentTopic.url, {
						value: value,
						mode: target.mode,
						preferences: target.preferences,
					}, true);
				}
			}
		}
	}
}

function check_motion(topic, payload) {
	if (payload.motion !== undefined) {
		customEvents.motionEvent(topic, payload.motion);
	} else
	if (Object.values(motionTopics).filter((item) => item.url === topic).length) {
		const motionTopicName = Object.keys(motionTopics).filter((item) => motionTopics[item].url === topic)[0];
		customEvents.motionEvent(motionTopicName, motionTopics[motionTopicName].data(db.getState()).motion);
	}
}

if (serverParams.proxy) {
	const io = require('socket.io-client');
	const proxy = require('../db/proxy.js');

	const socket = io(proxy.server, {
		auth: {
			hash: proxy.hash,
		},
	});
	socket
		.on('connect', () => {
			console.log(`Connected to server ${proxy.server}`);
		})
		.on('disconnect', () => {
			console.log(`Disconnected from server ${proxy.server}`);
		})
		.on('message', (data) => {
			console.log('proxy message', data);
			const topic = `${data.room}/controls/${data.target}`;
			const payload = {};
			['mode', 'value'].forEach((key) => {
				if (data[key]) payload[key] = Number(data[key]);
			});
			if (db.updateStateByTopic(topic, payload, true)) {
				check_motion(topic, payload);
			}
		});
}
