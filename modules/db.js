const fs = require('fs');
let state;
let state_saved;
let translation;

module.exports = {
	init() {
		return Promise.all([initState(), initTranslation()]);
	},
	getState() {
		return state;
	},
	saveState() {
		const state_json = JSON.stringify(state, null, 2);
		if (state_saved !== state_json) {
			fs.writeFile(serverParams.db_filename, state_json, 'utf8', (err) => {
				if(err) {
					log(err);
				} else {
					state_saved = state_json;
					log('state saved');
				}
			});
		}
	},
	updateStateByTopic(topic, payload, mqtt_publish) {
		const state_target = topic.split('/').reduce((prevValue, current) => {
			if(prevValue && prevValue[current]) {
				return prevValue[current];
			} else {
				return;
			}
		}, state);
		if(state_target) {
			for (let key in payload) {
				if (key === 'value') {
					const { min, max } = state_target.preferences;
					if (min !== undefined && max !== undefined) payload[key] = Math.min(max, Math.max(min, payload[key]));
				}
				state_target[key] = payload[key];
			}
			if(mqtt_publish) {
				mqttServer.publish(topic, payload);
			}
			return true;
		}
	},
	getTranslation() {
		return translation;
	},
	saveTranslation(callback) {
		fs.writeFile(serverParams.translate_filename, JSON.stringify(translation, null, '  '), 'utf8', (err) => {
			if(err) {
				log(err);
			} else {				
				log('translation saved');
				callback();
			}
		});
	},
	updateTranslation(data) {
		translation = data;
	},
	defaultState(){
		return {
			kitchen: {
				data: {
					floor: 0,
					floor2: 0
				},
				controls: {
					light: {
						type: 'slider',
						modes: ['off','on'],
						value: 100,
						mode: 0,						
						preferences: {
							hz: 1500,
							mute: 1
						}
					},
					light2: {
						type: 'slider',
						modes: ['off','on'],
						value: 100,
						mode: 0,
						preferences: {
							hz: 1500,
							mute: 1
						}
					},
					table: {
						type: 'slider',
						modes: ['off','on'],
						value: 100,
						mode: 0,
						preferences: {
							hz: 1500,
							mute: 1
						}
					},
					backlight: {
						type: 'slider',
						modes: ['off','on'],
						value: 100,
						mode: 0,
						preferences: {
							hz: 1500,
							mute: 1
						}
					},
					floor: {
						type: 'slider',
						modes: ['off','on'],
						value: 20,
						mode: 0,
						preferences: {
							min: 25,
							max: 30,
							delta: 1,
							title: '°',
							mute: 0
						}
					},
					floor2: {
						type: 'slider',
						modes: ['off','on'],
						value: 20,
						mode: 0,
						preferences: {
							min: 25,
							max: 30,
							delta: 1,
							title: '°',
							mute: 0
						}
					}
				}
			},
			bedroom: {
				controls: {
					light: {
						type: 'slider',
						modes: ['off','on'],
						value: 100,
						mode: 0,
						preferences: {
							hz: 1500,
							mute: 1
						}
					},
					light2: {
						type: 'slider',
						modes: ['off','on'],
						value: 100,
						mode: 0,
						preferences: {
							hz: 1500,
							mute: 1
						}
					},
					wardrobe: {
						type: 'slider',
						modes: ['off','on','auto'],
						value: 100,
						mode: 0,
						preferences: {
							hz: 1500,
							mute: 1
						}
					},
					fan: {
						type: 'slider',
						modes: ['off','on'],
						value: 50,
						mode: 1,
						preferences: {
							hz: 25000
						}
					}
				}
			},
			playroom: {
				controls: {
					light: {
						type: 'slider',
						modes: ['off','on'],
						value: 100,
						mode: 0,
						preferences: {
							hz: 1500,
							mute: 1
						}
					},
					light2: {
						type: 'slider',
						modes: ['off','on'],
						value: 100,
						mode: 0,
						preferences: {
							hz: 1500,
							mute: 1
						}
					}
				}
			},
			hall: {
				data: {
					motion: 0,
					floor: 0
				},
				controls: {
					light: {
						type: 'slider',
						modes: ['off','on'],
						value: 100,
						mode: 0,
						preferences: {
							hz: 1500,
							mute: 1
						}
					},
					light2: {
						type: 'slider',
						modes: ['off','on','auto'],
						value: 100,
						mode: 0,
						preferences: {
							hz: 1500,
							mute: 1,
							delay: 30,
							night_start: 22,
							night_end: 6,
							night_value: 10
						}
					},
					floor: {
						type: 'slider',
						modes: ['off','on'],
						value: 20,
						mode: 0,
						preferences: {
							min: 25,
							max: 30,
							delta: 1,
							title: '°',
							mute: 0
						}
					}
				}
			},
			bathroom: {
				data: {
					motion: 0,
					floor: 0
				},
				controls: {
					light: {
						type: 'slider',
						modes: ['off','on','auto'],
						value: 100,
						mode: 0,
						preferences: {
							hz: 1500,
							mute: 1,
							delay: 180,
							night_start: 22,
							night_end: 6,
							night_value: 10
						}
					},
					backlight: {
						type: 'slider',
						modes: ['off','on'],
						value: 100,
						mode: 0,
						preferences: {
							hz: 1500,
							mute: 1
						}
					},
					floor: {
						type: 'slider',
						modes: ['off','on'],
						value: 20,
						mode: 0,
						preferences: {
							min: 25,
							max: 30,
							delta: 1,
							title: '°',
							mute: 0
						}
					},
					fan: {
						type: 'slider',
						modes: ['off','on'],
						value: 50,
						mode: 1,
						preferences: {
							hz: 25000
						}
					}
				}
			},
			toilet: {
				data: {
					motion: 0,
					floor: 0
				},
				controls: {
					light: {
						type: 'slider',
						modes: ['off','on','auto'],
						value: 100,
						mode: 0,
						preferences: {
							hz: 1500,
							mute: 1,
							delay: 300,
							night_start: 22,
							night_end: 6,
							night_value: 10
						}
					},
					floor: {
						type: 'slider',
						modes: ['off','on'],
						value: 20,
						mode: 0,
						preferences: {
							min: 25,
							max: 30,
							delta: 1,
							title: '°',
							mute: 0
						}
					},
					fan: {
						type: 'slider',
						modes: ['off','on'],
						value: 50,
						mode: 1,
						preferences: {
							hz: 25000
						}
					}
				}
			},
			balcony: {
				data: {
					floor: 0
				},
				controls: {
					floor: {
						type: 'slider',
						modes: ['off','on'],
						value: 20,
						mode: 0,
						preferences: {
							min: 0,
							max: 30,
							delta: 1,
							title: '°',
							mute: 0
						}
					}
				}
			},
			// kitchen: {
			// 	controls: {
			// 		light: {
			// 			type: 'btn',
			// 			modes: ['off','on'],
			// 			mode: 0,
			// 			preferences: {
			// 				mute: 1
			// 			}
			// 		},
			// 		light_2: {
			// 			type: 'slider',
			// 			modes: ['off','on'],
			// 			value: 0,
			// 			mode: 0,
			// 			preferences: {
			// 				hz: 1000,
			// 				mute: 1
			// 			}
			// 		}
			// 	}
			// },
			// hall: {
			// 	controls: {
			// 		light: {
			// 			type: 'slider',
			// 			modes: ['off','on'],
			// 			value: 0,
			// 			mode: 0,
			// 			preferences: {
			// 				mute: 1
			// 			}
			// 		}
			// 	}
			// },
			// bathroom: {
			// 	data: {
			// 		t: 0,
			// 		h: 0
			// 	},
			// 	controls: {
			// 		fan: {
			// 			type: 'slider',
			// 			modes: ['off','on','auto'],
			// 			value: 0.35,
			// 			mode: 1,
			// 			preferences: {
			// 				hz: 25000,
			// 				h_min: 40,
			// 				h_max: 100,
			// 				pwm_min: 30,
			// 				pwm_max: 70
			// 			}
			// 		},
			// 		light: {
			// 			type: 'btn',
			// 			modes: ['off','on','auto'],
			// 			mode: 0,
			// 			preferences: {
			// 				hz: 1000,
			// 				delay: 300,
			// 				mute: 1
			// 			}
			// 		}
			// 	}
			// },
			// toilet: {
			// 	controls: {
			// 		fan: {
			// 			type: 'slider',
			// 			modes: ['off','on'],
			// 			value: 0.35,
			// 			mode: 1,
			// 			preferences: {
			// 				hz: 25000
			// 			}
			// 		}
			// 	}
			// }
		}
	},
	defaultTranslation(){
		return {
			langs:[],
			items:[]
		}
	}
}




function initState() {
	return new Promise((resolve, reject) => {
		try {
			fs.readFile(serverParams.db_filename, 'utf8', (err, data) => {
				if(err) {
					state = db.defaultState();
					log('default state init');
				} else {
					state = JSON.parse(data);
					log('state successfully loaded');
				}
				state_saved = JSON.stringify(state, null, 2);
				resolve();
			})
		} catch(err) {log(err)}
	})
}
function initTranslation() {
	return new Promise((resolve, reject) => {
		try {
			fs.readFile(serverParams.translate_filename, 'utf8', (err, data) => {
				if(err) {
					translation = db.defaultTranslation();
					log('default translation init');
				} else {
					translation = JSON.parse(data);
					log('translation successfully loaded');
				}
				resolve();
			})
		} catch(err) {log(err)}
	})
}