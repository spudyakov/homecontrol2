'use strict';

const functions = require('./modules/functions')

global.serverParams = {
	http_port: 80,
	mqtt_port: 1883,
	mqtt_http_port: 1884,
	ip: functions.getIp(),
	db_filename: './db/state.json',
	translate_filename: './db/translate.json',
	proxy: true,
}
global.log = functions.log;
global.db = require('./modules/db');
global.httpServer = require('./modules/httpServer');
global.mqttServer = require('./modules/mqttServer');


db.init()
	.then(() => {
		return httpServer.init();
	})
	.then(() => {
		return mqttServer.init();
	})
	.then(() => {
		log('All is ready');
		setInterval(() => {
			db.saveState();
		}, 60 * 60 * 1000);
	})