Vue.component('v-slider', {
	props: ['control_value','control_key'],
	data() {
		return {
			moving: false,
			value: null,
			min: null,
			max: null,
			title: null,			
			$slider_btn: null,
			$slider_area: null,
		}
	},
	template: `
		<div class="control_slider bg-before-border" @click="clickEvent">
			<div class="control_slider_size">
				<div class="control_btn bg-before-bg active_style" :class="moving?'moving':''" :style="'left:'+sliderPosition+';'">
					<div class="title" v-html="sliderTitle"></div>
				</div>
			</div>
		</div>`,
	computed: {
		isActive() {
			return this.control_value.modes[this.control_value.mode] == 'on';
		},
		sliderPosition() {
			return 100 * (this.value - this.min) / (this.max-this.min) + '%';
		},
		sliderTitle() {
			return this.value + this.title;
		},
	},
	watch: {
		'control_value.value': {
			immediate: true,
			handler(newVal, oldVal) {
				this.value = Number(newVal);
			},
		},
		'control_value.preferences': {
			immediate: true,
			handler(newVal, oldVal) {
				this.min = newVal.min===undefined?0:Number(newVal.min);
				this.max = newVal.max===undefined?100:Number(newVal.max);
				this.title = newVal.title===undefined?'%':''+newVal.title;
				this.value = Math.round(Math.max(this.min, Math.min(this.max, this.value)));
			},
		},
	},
	methods: {
		clickEvent(e) {
			if (!this.moving && this.isActive) {
				this.changeValue((e.pageX - this.$slider_area.getBoundingClientRect().left - 35) / this.$slider_area.offsetWidth);
				this.sendValue();
			}
		},
		changeValue(value) {
			this.value = Math.round(Math.max(this.min, Math.min(this.max, (this.min + (this.max - this.min) * value))));
		},
		sendValue() {
			sendMessage(this.$parent.page+'/controls/'+this.control_key, {
				value: this.value,
				mode: this.control_value.mode,
				// preferences: this.control_value.preferences,
			});
		},
	},
	mounted() {
		let pan_start_pos, parent_width;
		this.$slider_btn = this.$el.querySelector('.control_btn');
		this.$slider_area = this.$el.querySelector('.control_slider_size');
		const hammer = new Hammer(this.$slider_btn);
		hammer
			.on('panstart', (e) => {
				if (this.isActive) {
					pan_start_pos = this.$slider_btn.offsetLeft;
					parent_width = this.$slider_area.offsetWidth;
					panning = true;
					this.moving = true;
				}
			})
			.on('panmove', (e) => {
				if (this.isActive) {
					this.changeValue((pan_start_pos + e.deltaX) / parent_width);
				}
			})
			.on('panend', (e) => {
				if (this.isActive) {
					this.sendValue();
				}
				panning = false;
				setTimeout(() => {
					this.moving = false;
				}, 0);
			})
			.get('pan').set({threshold: 0})
	},
})