'use strict';

let panning = false;
const startPage = window.location.pathname.slice(1);
const hammer = new Hammer(document.querySelector('body'), {
	domEvents: true
});
const client = mqtt.connect('ws://'+window.location.hostname+':'+port);

let _translate = {};
translate.items.forEach((el) => {
	let items = {};
	el[1].forEach((item, indx) => {
		items[translate.langs[indx]] = item;
	})
	_translate[el[0]] = items;
})

const app = new Vue({
	el: document.querySelector('#app'),
	data: {
		preferencesIsActive: false,
		page: startPage,
		pageIsActive: startPage!='',
		state: state,
		lang: translate.langs[0],
		translate: _translate
	},
	methods: {
		preferencesToggle() {
			this.preferencesIsActive ^= true;
		},
		muteLight() {
			const message = { event: 'muteLight' };
			if(this.pageIsActive) {
				message.page = this.page;
			}
			sendMessage('custom-events', message);
		},
		reboot() {
			const message = { reason: client.options.clientId };
			sendMessage('watchdog/reboot', message);
		},
		pageChange(page) {
			if(!page) {
				this.pageIsActive = false;
				page = '';
				panning = false;
			} else {
				this.pageIsActive = true;
				this.page = page;
			}
			page = '/'+page;
			if(page != window.location.pathname) {
				history.pushState(page, null, page);
			}
		},
		modeToggle(control_key) {
			let mode = this.state[this.page].controls[control_key].mode+1;
			if(mode >= this.state[this.page].controls[control_key].modes.length) {
				mode = 0;
			}
			sendMessage(this.page+'/controls/'+control_key, {
				value: this.state[this.page].controls[control_key].value,
				mode: mode,
				// preferences: this.state[this.page].controls[control_key].preferences,
			});
		},
		savePreferences(control_key) {
			sendMessage(this.page+'/controls/'+control_key, {
				preferences: this.state[this.page].controls[control_key].preferences,
			});
		},
		getPreferencesWidth: (preferences) => {
			return 'width: '+100/Object.keys(preferences).length+'%;';
		},
		getInputType(value) {
			return Number(value)==value&&value!=''?'number':'text';
		}
	}
})

window.onpopstate = (event) => {
	app.pageChange(event.state?event.state.slice(1):startPage);
}

client
	.on('connect', () => {
		console.log(`connected as ${client.options.clientId} to ${client.options.host}`);
	})
	.on('message', (topic, message) => {
		console.log(`get message at ${topic}: ${message}`);
		stateUpdate(topic, JSON.parse(message));
	})
	.subscribe('#')

hammer
	.on('swiperight', () => {
		if(app.pageIsActive && !panning) {
			app.pageChange();
		}
	})
	.on('swipeleft', () => {
		if(!app.pageIsActive && app.page) {
			app.pageChange(app.page);
		}
	})
	.on('tap press', (e) => {
		const clickEvent = e.target.getAttribute('clickEvent');
		const clickData = e.target.getAttribute('clickData');
		if(clickEvent && app[clickEvent]) {
			app[clickEvent](clickData);
		}
	})



function stateUpdate(topic, payload) {
	const state_target = topic.split('/').reduce((prevValue, current) => {
		if(prevValue && prevValue[current]) {
			return prevValue[current];
		} else {
			return;
		}
	}, app.state);
	if(state_target) {
		for (let key in payload) {
			state_target[key] = payload[key];
		}
	}
}
function sendMessage(topic, message) {
	message = JSON.stringify(message);
	client.publish(topic, message);
	console.log(`send message to ${topic}: ${message}`);
}