var myApp = angular.module('myApp', []);
myApp.controller('translationsCtrl', function($scope, $http) {
	$scope.translations = translate;
	$scope.save_success = false;
	$scope.save_error = false;
	$scope.remove_mode = false;

	$scope.add_row = function() {
		$scope.translations.items.push(['', $scope.translations.langs.map(function(){
			return '';
		})]);
	}
	$scope.add_col = function() {
		$scope.translations.langs.push('');
	}
	$scope.save = function() {
		$http.post("/translate", $scope.translations)
			.then(function (response) {
				if(response.data.status!='success') {
					setStatus('save_error');
				} else {
					setStatus('save_success');
				}
			});
	}
	$scope.remove_mode_toggle = function() {
		$scope.remove_mode ^= true;
	}
	$scope.remove_row = function(id) {
		$scope.translations.items.splice(id, 1);
	}
	$scope.remove_col = function(id) {
		if(confirm('Удалить столбец переводов '+$scope.translations.langs[id]+'?')) {
			$scope.translations.langs.splice(id, 1);
			$scope.translations.items.forEach(function(item){
				item[1].splice(id, 1);
			})
		}
	}
	
	function setStatus(status) {
		$scope[status] = true;
		setTimeout(function(){
			$scope[status] = false;
			$scope.$apply();
		}, 300)
	}
});